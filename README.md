# Depfu example Rails app

[![Depfu](https://badges.depfu.com/badges/2dd3725961dbe0c8a06e9b09024e6945/status.svg)](https://depfu.com)
[![Depfu](https://badges.depfu.com/badges/2dd3725961dbe0c8a06e9b09024e6945/overview.svg)](https://depfu.com/github/depfu/example-ruby)
[![Depfu](https://badges.depfu.com/badges/2dd3725961dbe0c8a06e9b09024e6945/count.svg)](https://depfu.com/github/depfu/example-ruby)

This is a simple Rails app with a few outdated dependencies.

Take a look at the [open merge requests](https://gitlab.com/depfu/example-ruby/merge_requests) to see how Depfu works.
